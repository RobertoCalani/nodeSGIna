const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const session = require('express-session')
app.use(session({
	secret: '!#abcñÑChiiviito%&1',
	saveUninitialized: false,
	resave: false,
	rolling: true,
	cookie: {
		maxAge: 3600000 * 24
	}
}));


// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// SGIna
const loadModules = require('./app/loadModules');
const loadComponents = require('./app/loadComponents');
const getRoutes = require('./app/getRoutes');
const getPermission = require('./app/getPermission');
const makeModels = require('./app/makeModels');
const loadResourcesNeed = require('./app/loadResourcesNeeded');

// Views backend
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/public/views'));
// Get routes
getRoutes(app); // Obtiene todas las rutas de los modulos para AngularJs

app.use('/res' ,express.static(path.join(__dirname, '/public/res')));
let model = {};
// Prepara todo y luego manda
makeModels(app, model, (cnn, model) => {
	loadModules(app, model, (controllers, permissions) => {
		getPermission(app, permissions, model, (listaDePermisos) => {
			// Obtiene todas las rutas de los modulos para AngularJs
			// Le envio una lista con los modulos para luego leerlos y ver que componentes son los que se utilizan en la ruta seleccionada
			loadComponents(app, (components) => {
				app.get('/(:moduleSGIna)?(/*)?', (req, res) => {
					// Luego de leer el modulo al que se esta entrando se debe cargar los controladores, directivas u otros componentes
					loadResourcesNeed(app,controllers, req.params.moduleSGIna, (resources) => {
						res.render('index', { controllers: resources.controllers, components: components, directives: resources.directives });
						//res.send(controllers);
					});	
				});
			});
		});
	});
});

app.listen(8080, ()=> {
	console.log('Run port 8080!');
});