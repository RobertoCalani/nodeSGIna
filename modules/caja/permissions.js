module.exports = [
	{
		name: 'Acceso',
		permission: 'caja.acceso'
	},
	{
		name: 'Editar',
		permission: 'caja.editar'
	}
]