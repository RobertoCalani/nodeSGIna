/**
 * 
 * 	No importa el modelo que sea, todos tienen las mismas funciones:
 * 
 * 		- GET  - /test				:: 	Trae una lista de todos los registros
 * 		- GET  - /test/:_id			::	Trae ese unico registro si es que existe
 * 		- POST - /test/:_id			::	Actualiza ese registro, dependiendo de los campos que se envie
 * 
 */
module.exports = [
	{
		name: 'Contacto', // Nombre del modelo
		nameMongoDB: 'contactos', // Nombre de la coleccion en MongoDB
		schema: {
			nombre: String,
			apellidos: {
				type: String,
				uppercase: true
			},
			pais: String,
			direccion: String,
			provincia: String,
			tipoContacto: String,
			cp: String,
			localidad: String,
			correos: [],
			telefonos: [],
			fechaAlta: {
				type: Date,
				default: Date.now(),
				require: true
			},
			eliminado: false
		}
	}
]