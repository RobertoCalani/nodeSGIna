module.exports = {
	name: 'Contactos',
	nameRoute: 'contactos', // Nombre de la carpeta, se usa en el backend
	icon: 'icon-user',
	controllers: [
		'ContactoController.js'
	],
	require: [
		/*
			Los require tienen que ser: 'components', 'modules', 'directive'
		*/
		"public/components/numeroaletras/numeroaletras.js",
		"modules/ventas/frontend/controllers/VentasController.js",
	]
}