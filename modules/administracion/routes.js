/**
 * Todas las rutas de template o rutas de navegador empiezan con un / y termina sin nada
 */
module.exports = [
	{
		route: '/',
		title: 'Administracion',
		template: '/index.html',
		navBar: '/navBar.html'
	},
	{
		route: '/permisos',
		title: 'Administracion - Permisos',
		template: '/permisos.html',
		navBar: '/navBar.html'
	},
	{
		route: '/usuarios',
		title: 'Administracion - Usuarios',
		template: '/usuarios.html',
		navBar: '/navBar.html'
	}
]