module.exports = {
	name: 'Mi panel',
	nameRoute: 'usuario', // Por defecto viene el nombre del modulo en minusculas y/o separados por guiones
	icon: 'icon-cog',
	controllers: [
		'/UsuarioController.js'
	],
	require: [
		/*
			Los require tienen que ser: 'components', 'modules', 'directive'
		*/
		"components/momentsjs/js:momentjs.min.js",
		"components/numeroaletras/numeroaletras.js",
		"modules/ventas/controller/VentasController.js",
	]
}