/**
 * 
 * 	No importa el modelo que sea, todos tienen las mismas funciones:
 * 
 * 		- GET  - /test				:: 	Trae una lista de todos los registros
 * 		- GET  - /test/:_id			::	Trae ese unico registro si es que existe
 * 		- POST - /test/:_id			::	Actualiza ese registro, dependiendo de los campos que se envie
 * 
 */
module.exports = [
	{
		name: 'UsuarioPermisos', // Nombre del modelo
		nameMongoDB: 'usuarios.permisos', // Nombre de la coleccion en MongoDB
		schema: {
			username: String,
			permissions: []
		}
	},
	{
		name: 'Usuario', // Nombre del modelo
		nameMongoDB: 'usuarios', // Nombre de la coleccion en MongoDB
		schema: {
			username: String,
			password: String,
			fechaAlta: {
				type: Date,
				default: Date.now,
				require: true
			},
			contactoId: String
		}
	}
]