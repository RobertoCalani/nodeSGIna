/**
 * Se debe respetar el orden de carga de estos componentes
 */
module.exports = [
	{
		name: 'JQuery',
		js: '/jquery/jquery-3.1.1.min.js'
	},
	{
		name: 'angular',
		js: [
			'/angular/angular.min.js',
			'/angular/angular-route.min.js',
			'/angular/angular-resource.min.js'
		]
	},
	{
		name: 'popper',
		js: '/popper/popper.min.js'
	},
	{
		name: 'bootstrap',
		js: '/bootstrap/js/bootstrap.min.js',
		css: '/bootstrap/css/bootstrap.min.css'
	},
	{
		name: 'fontello',
		css: ['/fontello/css/animation.css', '/fontello/css/icon-.css']
	},
	{
		name: 'moments',
		js: '/momentjs/moment.js'
	},
	{
		name: 'select2',
		js: ['/select2/select2.min.js', '/select2/load-select2.js'],
		css: ['/select2/select2.min.css', '/select2/select2-bootstrap4.min.css']
	}
]