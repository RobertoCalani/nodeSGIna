const path = require('path');
const fs = require('fs');
const slugify = require('slugify');
const clone = require('clone');

module.exports = function (app, permissions, model, call) {
	let permisos = [];
	app.post('/sgina/get-permissions', (req, res) => {
		// Solo si es admin, agregar condicion de permiso
		res.json(permissions);
	});

	console.log(':: Permissions loaded');
	call(permisos);
}