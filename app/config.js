module.exports = {
    // Servidor
    port: 8080,
    // MongoDB
    mongo: {
        database: "sgina",
        hostname: "127.0.0.1",
        auth: false,
        username: "",
        password: "",
        autoreconnect: true,
        port: 27017
    }
}