const fs = require('fs');
const express = require("express");
const path = require('path');

module.exports = function(app, controllers, modul, call) {    
    
    let resources = {
        controllers: [],
        components: [],
        directives: []
    }
    // Pantalla principal
    // En este modulo no toma en cuenta el config.js
    if(!modul) {
        resources.controllers.push('module/main/controllers/MainController.js');
    }
    if(fs.existsSync(path.join(__dirname, '/../modules/'+modul))) {
        let config = require(path.join(__dirname, '/../modules/' + modul + '/config.js'));
        // Primero lee los controler de si mismo
        config.controllers.forEach(c => {
            if(fs.existsSync(path.join(__dirname, '/../modules/' + modul + '/frontend/controllers/' + c))) {
                resources.controllers.push(("module/" + modul + '/controllers/' + c).replace(/(\/\/)/g, '/'));
                //console.log("Se metio folder", ("module/" + modul + '/controllers/' + c).replace(/(\/\/)/g, '/'));
            }
        });

        // Carga los controladores requeridos
        if(config.require) { 
            config.require.forEach(c => {
                if(fs.existsSync(path.join(__dirname, '/../' + c))) {
                    controllers.forEach(cl => {
                        if(c==cl[0]) {
                            resources.controllers.push(cl[1]);
                        }
                    });
                }
            });
        }
        

    }
    call(resources);
}